terraform {
  required_version = "0.13.5"

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "3.18.0"
    }
  }

  # TODO: [last_name]を修正
  backend "s3" {
    bucket         = "suzuki-staging-terraform-tfstate"
    key            = "public_traffic_container_service_notifier/terraform.tfstate"
    encrypt        = true
    dynamodb_table = "suzuki_terraform_tfstate_lock"

    # TODO: role_arnのロール名 [LastName]を修正
    role_arn = "arn:aws:iam::938285887320:role/SuzukiStagingTerraformBackendAccessRole"
  }
}
