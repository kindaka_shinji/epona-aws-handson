terraform {
  required_version = ">=0.13.3"

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "3.18.0"
    }
  }

  backend "s3" {
    bucket         = "yamada-staging-terraform-tfstate"
    key            = "cd_pipeline_frontend/terraform.tfstate"
    encrypt        = true
    dynamodb_table = "yamada_terraform_tfstate_lock"

    role_arn = "arn:aws:iam::938285887320:role/YamadaStagingTerraformBackendAccessRole"
  }
}
