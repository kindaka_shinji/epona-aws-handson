#!/bin/bash -eu

SCRIPT_DIR=$(cd $(dirname $0); pwd)
REPOSITORY_ROOT=$(git rev-parse --show-toplevel)

USERS="$@"
. ${SCRIPT_DIR}/common.sh

check_tools

DELIVERY_INSTANCES_1=$(cat ${SCRIPT_DIR}/sequences/apply.delivery.sequence.1 | grep -v '^#')
DELIVERY_INSTANCES_2=$(cat ${SCRIPT_DIR}/sequences/apply.delivery.sequence.2 | grep -v '^#')
RUNTIME_INSTANCES_1=$(cat ${SCRIPT_DIR}/sequences/apply.runtime.sequence.1 | grep -v '^#')
RUNTIME_INSTANCES_2=$(cat ${SCRIPT_DIR}/sequences/apply.runtime.sequence.2 | grep -v '^#')

function apply_pattern_instances() {
  local user_name=$1
  local pattern_instances=${@:2}

  for instance_dir in ${pattern_instances}; do
    apply_pattern_instance ${user_name} ${instance_dir}
  done
}

for user in ${USERS}; do
  echo "apply delivery pattern: start"
  set_delivery_terraformer_credential ${user}
  apply_pattern_instances ${user} ${DELIVERY_INSTANCES_1[@]}
  echo "apply delivery pattern: done"

  echo "apply runtime pattern: start"
  set_staging_terraformer_credential ${user}
  apply_pattern_instances ${user} ${RUNTIME_INSTANCES_1[@]}
  SMTP_OUTPUT=$(terraform output -json)
  SMTP_USER=$(echo ${SMTP_OUTPUT} | jq -r .smtp_user.value.aws_iam_access_key)
  SMTP_PASSWORD=$(echo ${SMTP_OUTPUT} | jq -r .smtp_user.value.aws_iam_smtp_password_v4)

  # parameter_store/terraform.tfvarsファイルの書き換え
  # 値はハードコード
  # SMTP_PASSWORDだけスラッシュが含まれる可能性があるため、セパレータを@に変更
  sed -i.org \
    -e "4s/xxxxxxx/${user}/g" \
    -e "5s/xxxxxxx/password/g" \
    -e "10s/XXXXXX/${SMTP_USER}/g" \
    -e "11s@XXXXXX@${SMTP_PASSWORD}@g" \
    -e "12,13s/XXXXXX@xxxx.com/kiri.yuichi@tis.co.jp/g" \
    -e "s/redis-xxxx/redis-password123/g" \
    ../parameter_store/terraform.tfvars

  apply_pattern_instances ${user} ${RUNTIME_INSTANCES_2[@]}
  echo "apply runtime pattern: done"

  echo "re-apply delivery pattern: start"
  # cd_pipelineのkey_idを取得
  set_staging_terraformer_credential ${user}
  cd ${REPOSITORY_ROOT}/${user}/runtimes/staging/cd_pipeline_backend_notifier
  CD_PIPELINE_NOTIFIER_KEY_ID=$(terraform output -json | jq -r ".cd_pipeline_backend_notifier.value.kms_keys.keys[\"alias/${user}-ntfr-artfct_key\"].key_id")
  cd ${REPOSITORY_ROOT}/${user}/runtimes/staging/cd_pipeline_frontend
  CD_PIPELINE_FRONTEND_KEY_ID=$(terraform output -json | jq -r ".cd_pipeline_frontend.value.kms_keys.keys[\"alias/${user}-frnt-artifacts_key\"].key_id")

  # delivery/runtime_instancesのファイルを修正
  cd ${REPOSITORY_ROOT}/${user}
  sed -i "" \
    -e "33s/# \(.*\)xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx/\1${CD_PIPELINE_NOTIFIER_KEY_ID}/g" \
    delivery/runtime_instances/staging/cd_pipeline_backend_trigger_notifier/main.tf
  sed -i "" \
    -e "25s/# \(.*\)xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx/\1${CD_PIPELINE_FRONTEND_KEY_ID}/g" \
    delivery/runtime_instances/staging/cd_pipeline_frontend_trigger/main.tf

  set_delivery_terraformer_credential ${user}
  apply_pattern_instances ${user} ${DELIVERY_INSTANCES_2[@]}
  echo "re-apply delivery pattern: done"
done
