data "terraform_remote_state" "staging_public_traffic_container_service_notifier" {
  backend = "s3"

  config = {
    bucket         = "kato-staging-terraform-tfstate"
    key            = "public_traffic_container_service_notifier/terraform.tfstate"
    encrypt        = true
    dynamodb_table = "kato_terraform_tfstate_lock"

    role_arn = "arn:aws:iam::938285887320:role/KatoStagingTerraformBackendAccessRole"
  }
}
