provider "aws" {
  assume_role {
    # 実行権限を定義したロール
    role_arn = "arn:aws:iam::922032444791:role/KatoTerraformExecutionRole"
  }
}

module "smtp_user" {
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/components/iam/ses_smtp_user?ref=v0.2.1"

  username    = "KatoSesSmtpUser"
  policy_name = "KatoSesSmtpPolicy"

  tags = {
    Owner              = "kato"
    Environment        = "runtime"
    RuntimeEnvironment = "staging"
    ManagedBy          = "epona"
  }
}
